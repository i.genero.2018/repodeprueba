# Programa que calcula los factoriales de los números entre el 1 y 10 (incluidos)
# y los muestra en pantalla
# El factorial de un número es el resultado de multiplicar ese número
# por todos los anteriores desde el 1
def calcular_factorial(num): # Función recursiva
    if num <= 1:        # El factorial de 0 y 1 es 1
        return 1
    else:               # El factorial del resto de números es ese número multiplicado por todos los anteriores
        return num * calcular_factorial(num - 1)
# El bucle termina cuando la función devuelve 1

# El programa empieza aquí ejecutando el bucle hasta calcular los factoriales del 1 al 10 incluido
for i in range(1, 11):
    resultado = calcular_factorial(i)   # Calcula el factorial de cada número
    print(f"El factorial de {i} es {resultado}") # Imprime el resultado por pantalla
#El programa finaliza aquí cuando el bucle termina
